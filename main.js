// Find all the input elements
const inputs = document.querySelectorAll('input');

// Loop through them and attach the click listener
inputs.forEach(el => {
  el.addEventListener('click', yesYouCan);
});

// Function to run onclick of ALL inputs (even disabled ones)
function yesYouCan(e) {
  alert('Yes you can!');
}