Can I Click It
===============

The Challenge
-------------

To fire the function `yesYouCan` when the disabled input is clicked.

Guidance & Rules
----------------

1. jQuery is allowed, feel free to use it if you want. But...
2. The less code the better. If you can do it without a library, go for it.
3. The current code would work for all inputs that aren't disabled. Your code
   needs to work for all inputs _including_ disabled ones. Your code does not
   necessarilly have to look like the code that is there now but a solution is
   achievable without changing too much.

Instructions
------------

Clone this repository and then create a branch named after your name (mike, 
benji, kat etc...) in which to work on your solution. On the day of the 
challange you can push your branch to this repository so everyone can see 
your solution.

Finally
-------
This is a bit of fun. If you don't come up with a solution don't worry. If 
you need help with the GIT side of things or if you have any questions at 
all speak to me (Mike) or Kat.

Good luck!

https://youtu.be/O3pyCGnZzYA
